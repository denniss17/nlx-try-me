# (Re)generate certificates

## Init CA

```
cd pki/ca && \
    cfssl gencert -initca ../csr-ca.json | cfssljson -bare ca
```

## Init intermediate CA

```
cd pki/ca && \
    cfssl gencert -initca ../intermediate-ca.json | cfssljson -bare intermediate_ca && \
    cfssl sign -ca ca.pem -ca-key ca-key.pem -config ../cfssl.json -profile intermediate_ca intermediate_ca.csr | cfssljson -bare intermediate_ca
```

## Create host certificate

```
cd pki/certs && \
    cfssl gencert -ca ../ca/intermediate_ca.pem -ca-key ../ca/intermediate_ca-key.pem -config ../cfssl.json -profile=server ../management-api.json | cfssljson -bare management-api.try-me.nlx.local
```
