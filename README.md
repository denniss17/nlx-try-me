# NLX Try Me

This repository contains the files required to follow our [Try NLX guide](https://docs.nlx.io/try-nlx/management/introduction/).
After following this guide you will have a working NLX setup connected to our demo environment.

Please note that the NLX setup in this respository is made for demo purposes and should not be used as a production setup.

This repository contains files required to run the demo environment.
For more information please check the [documentation](https://docs.nlx.io/try-nlx/management/introduction/)
